function main() {
  var clickerSpreadsheet = getClickerSpreadsheet();
  var unitNames = getUnitNamesFromQuestions(clickerSpreadsheet);
  // Create slide deck for each unit
  // Due to API quota, you need to limit this to only loop once or twice
  for (var unitIdx = 0; unitIdx < 9; unitIdx++) {
    var unitTitle = unitNames[unitIdx];
    var questions = clickerSpreadsheet.filter(function(row) { return row[0] == unitNames[unitIdx] });
    createSlideDeckForUnit(unitTitle, questions);
  }
}

function getClickerSpreadsheet() {
  var spreadsheetId = '1_AUEwxeq5mJaw4CKwjGf5an5u5kz8VmzNWZG_WV2l9Q';
  var rangeName = 'Questions!A2:K';
  var values = Sheets.Spreadsheets.Values.get(spreadsheetId, rangeName).values;
  if (!values) {
    Logger.log('No data found.');
  } else {
    return values;
  }
}

function getUnitNamesFromQuestions(questions) {
  // Get the different unit names from the spreadsheet
  // Google Apps Script doesn't have Sets, so we have to hack an ugly solution
  var unitNamesSet = {};
  var unitNames = [];
  for (var row = 0; row < questions.length; row++) {
    var unitName = questions[row][0];
    if (unitName.length > 0) {
      if (!unitNamesSet[unitName]) {
        unitNamesSet[unitName] = true;
        unitNames.push(unitName);
      }
    }
  }
  return unitNames;
}

function createSlideDeckForUnit(unitTitle, questions) {
  var presentation = Slides.Presentations.create({
    title: unitTitle,
    slides: [],
  });
  var presentationId = presentation.presentationId;
  // Create the title slide
  
  // Generate the slides for each of the questions
  for (var qIdx = 0; qIdx < questions.length; qIdx++) {
     createQuestionSlide(presentationId, questions[qIdx]);
  }
}

function createQuestionSlide(presentationId, question) {
  // Create the question choices
  var questionOptionsBody = "";
  var firstBullet = true;
  var randomSeed = Math.floor((Math.random()*4)+1);
  for (var qIdx = 0; qIdx <= 4; qIdx++) {
    var curIdx = 3 + ((randomSeed + qIdx) % 5);
    if (question[curIdx].length > 0) {
      if (!firstBullet) {
        questionOptionsBody += '\n';
      }
      firstBullet = false;
      questionOptionsBody += question[curIdx];
    }
  }
  // Get UUIDs
  var pageObjectId = Utilities.getUuid();
  var titleId = Utilities.getUuid();
  var bodyId = Utilities.getUuid();
  // Create the slide
  // https://developers.google.com/slides/samples/slides#create_a_new_slide_and_modify_placeholders
  // https://developers.google.com/slides/reference/rest/v1/presentations/request#createparagraphbulletsrequest
  var requests = [{
    createSlide: {
      objectId: pageObjectId,
      slideLayoutReference: {
        predefinedLayout: 'TITLE_AND_BODY'
      },
      placeholderIdMappings: [
        {
          layoutPlaceholder: {
            type: 'TITLE',
            index: 0,
          },
          objectId: titleId,
        },
        {
          layoutPlaceholder: {
            type: 'BODY',
            index: 0,
          },
          objectId: bodyId,
        }
      ],
    },
  },
  {
    insertText: {
      objectId: titleId,
      text: question[2]
    }
  },
  {
    insertText: {
      objectId: bodyId,
      text: questionOptionsBody
    }
  },
  {
    createParagraphBullets: {
      objectId: bodyId,
      textRange: {
        type: 'ALL'
      },
      bulletPreset: "NUMBERED_UPPERALPHA_ALPHA_ROMAN"
    }
  }];
  // Execute the request
  const createSlideResponse = Slides.Presentations.batchUpdate({
    requests: requests
  }, presentationId);
  
  // Get the ID of the speaker's notes
  const curPage = Slides.Presentations.Pages.get(presentationId, pageObjectId);
  const speakerNotesObjectId = curPage.slideProperties.notesPage.notesProperties.speakerNotesObjectId;
  
  // Create the answer and question information in the speakers notes
  var speakersNotesText = "";
  // Add the answer
  if (question[8] && question[8].length > 0) {
    speakersNotesText += "ANSWER: " + question[8] + "\n\n";
  }
  else {
    speakersNotesText += "Answer: N/A \n\n";
  }
  // Add the rationale, if it exists
  if (question[10] && question[10].length > 0) {
    speakersNotesText += "Rationale for answer: " + question[8] + "\n";
  }
  // Add unit information
  if (question[0] && question[0].length > 0) {
    speakersNotesText += "Unit: " + question[0] + "\n";
  }
  if (question[1] && question[1].length > 0) {
    speakersNotesText += "Topic: " + question[1] + "\n";
  }
  // Create the request
  requests = [
    {
      insertText: {
        objectId: speakerNotesObjectId,
        text: speakersNotesText
      }
    }
  ]
  // Execute the request
  const createSpeakersNotesResponse = Slides.Presentations.batchUpdate({
    requests: requests
  }, presentationId);
}
