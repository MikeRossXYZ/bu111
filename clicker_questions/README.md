# Clicker question generator

After creating multiple choice "clicker" questions for the course, the instructors needed an easy way to translate these into presentation slides that could be presented for students in lectures and labs.

The following Google Apps Script program uses the Google Slides and Google Sheets APIs to read from a Sheet containing the questions and creates presentations as Slides files using the information from the sheet. The presentation files are created in the root folder of the user who runs the script.

In order to run, the user must update:

* Create a new Google Apps Script project at https://script.google.com
* The "spreadsheetId" variable with the ID of the Sheet you are trying to read from
* The indices of the columns within the spreadsheet must be updated as needed